<?php

namespace PayPal\Exception;

/**
 * Class PayPalMissingCredentialException
 *
 * @package PayPal\Exception
 */
class PayPalMissingCredentialException extends \Exception
{

    public function __construct(string $message = '', int $code = 0)
    {
        parent::__construct($message, $code);
    }

    /**
     * prints error message
     *
     * @return string
     */
    public function errorMessage(): string
    {
        return 'Error on line ' . $this->getLine() . ' in ' . $this->getFile()
            . ': <b>' . $this->getMessage() . '</b>';
    }
}
