<?php
namespace PayPal\Test\Common;

use PayPal\Api\Amount;
use PayPal\Api\Currency;
use PayPal\Api\Details;
use PayPal\Api\InvoiceItem;
use PayPal\Api\Item;
use PayPal\Api\Tax;
use PayPal\Common\PayPalModel;
use PayPal\Converter\FormatConverter;
use PayPal\Test\Validation\NumericValidatorTest;
use PHPUnit\Framework\TestCase;

class FormatConverterTest extends TestCase
{

    public static function classMethodListProvider(): array
    {
        return [
            '1' => [new Item(), 'Price'],
            '2' => [new Item(), 'Tax'],
            '3' => [new Amount(), 'Total'],
            '4' => [new Currency(), 'Value'],
            '5' => [new Details(), 'Shipping'],
            '6' => [new Details(), 'SubTotal'],
            '7' => [new Details(), 'Tax'],
            '8' => [new Details(), 'Fee'],
            '9' => [new Details(), 'ShippingDiscount'],
            '10' => [new Details(), 'Insurance'],
            '11' => [new Details(), 'HandlingFee'],
            '12' => [new Details(), 'GiftWrap'],
            '13' => [new InvoiceItem(), 'Quantity'],
            '14' => [new Tax(), 'Percent'],
        ];
    }

    public static function CurrencyListWithNoDecimalsProvider(): array
    {
        return [['JPY'], ['TWD'], ['HUF']];
    }

    public static function apiModelSettersProvider(): array
    {
        $provider = [];
        foreach (NumericValidatorTest::positivePriceProvider() as $value) {
            foreach (self::classMethodListProvider() as $method) {
                $provider[] = array_merge($method, [$value]);
            }
        }
        return $provider;
    }

    public static function apiModelSettersInvalidProvider(): array
    {
        $provider = [];
        foreach (NumericValidatorTest::invalidProvider() as $value) {
            foreach (self::classMethodListProvider() as $method) {
                $provider[] = array_merge($method, [$value]);
            }
        }
        return $provider;
    }

    /**
     *
     * @dataProvider \PayPal\Test\Validation\NumericValidatorTest::positiveProvider
     */
    public function testFormatToTwoDecimalPlaces($input, $expected): void
    {
        $result = FormatConverter::formatToNumber($input);
        $this->assertEquals($expected, $result);
    }

    /**
     * @dataProvider CurrencyListWithNoDecimalsProvider
     */
    public function testPriceWithNoDecimalCurrencyInvalid($input): void
    {
        try {
            FormatConverter::formatToPrice("1.234", $input);
        } catch (\InvalidArgumentException $ex) {
            $this->assertStringContainsString("value cannot have decimals for", $ex->getMessage());
        }
    }

    /**
     * @dataProvider CurrencyListWithNoDecimalsProvider
     */
    public function testPriceWithNoDecimalCurrencyValid($input): void
    {
        $result = FormatConverter::formatToPrice("1.0000000", $input);
        $this->assertEquals("1", $result);
    }

    /**
     *
     * @dataProvider \PayPal\Test\Validation\NumericValidatorTest::positiveProvider
     */
    public function testFormatToNumber($input, $expected): void
    {
        $result = FormatConverter::formatToNumber($input);
        $this->assertEquals($expected, $result);
    }

    public function testFormatToNumberDecimals(): void
    {
        $result = FormatConverter::formatToNumber("0.0", 4);
        $this->assertEquals("0.0000", $result);
    }


    public function testFormat(): void
    {
        $result = FormatConverter::format("12.0123", "%0.2f");
        $this->assertEquals("12.01", $result);
    }

    /**
     * @dataProvider apiModelSettersProvider
     *
     * @param PayPalModel $class Class Object
     * @param string $method Method Name where the format is being applied
     * @param array $values array of ['input', 'expectedResponse'] is provided
     */
    public function testSettersOfKnownApiModel($class, $method, $values): void
    {
        $obj = new $class();
        $setter = "set" . $method;
        $getter = "get" . $method;
        $result = $obj->$setter($values[0]);
        $this->assertEquals($values[1], $result->$getter());
    }

    /**
     * @dataProvider apiModelSettersInvalidProvider
     *
     */
    public function testSettersOfKnownApiModelInvalid($class, $methodName, $values): void
    {
        $this->expectException(\InvalidArgumentException::class);
        $obj = new $class();
        $setter = "set" . $methodName;
        $obj->$setter($values[0]);
    }
}
