<?php

use PayPal\Exception\PayPalInvalidCredentialException;
use PHPUnit\Framework\TestCase;

class PayPalInvalidCredentialExceptionTest extends TestCase
{
    protected PayPalInvalidCredentialException $object;

    protected function setUp():void
    {
        $this->object = new PayPalInvalidCredentialException();
    }

    public function testErrorMessage(): void
    {
        $msg = $this->object->errorMessage();
        $this->assertStringContainsString('Error on line', $msg);
    }
}
