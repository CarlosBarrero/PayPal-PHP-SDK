<?php

use PayPal\Exception\PayPalMissingCredentialException;
use PHPUnit\Framework\TestCase;

class PayPalMissingCredentialExceptionTest extends TestCase
{
    /**
     * @var PayPalMissingCredentialException
     */
    protected PayPalMissingCredentialException $object;

    protected function setUp():void
    {
        $this->object = new PayPalMissingCredentialException;
    }


    public function testErrorMessage(): void
    {
        $msg = $this->object->errorMessage();
        $this->assertStringContainsString('Error on line', $msg);
    }
}
