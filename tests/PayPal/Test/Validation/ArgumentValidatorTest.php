<?php
namespace PayPal\Test\Validation;

use PayPal\Validation\ArgumentValidator;
use PHPUnit\Framework\TestCase;

class ArgumentValidatorTest extends TestCase
{

    public static function positiveProvider(): array
    {
        return [
            ["1"],
            ["something here"],
            [1],
            [[1, 2, 3]],
            [0.123],
            [true],
            [false],
            [[]],
        ];
    }

    public static function invalidProvider(): array
    {
        return [
            [null],
            [''],
            ['     '],
        ];
    }

    /**
     * @dataProvider positiveProvider
     */
    public function testValidate($input): void
    {
        $this->assertTrue(ArgumentValidator::validate($input, "Name"));
    }

    /**
     * @dataProvider invalidProvider
     */
    public function testInvalidDataValidate($input): void
    {
        $this->expectException(\InvalidArgumentException::class);
        $this->assertTrue(ArgumentValidator::validate($input, "Name"));
    }
}
