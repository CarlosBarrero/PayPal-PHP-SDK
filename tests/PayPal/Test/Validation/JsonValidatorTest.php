<?php
namespace PayPal\Test\Validation;

use PayPal\Validation\JsonValidator;
use PHPUnit\Framework\TestCase;

class JsonValidatorTest extends TestCase
{

    public static function positiveProvider(): array
    {
        return [
            [null],
            [''],
            ["{}"],
            ['{"json":"value", "bool":false, "int":1, "float": 0.123, "array": [{"json":"value", "bool":false, "int":1, "float": 0.123},{"json":"value", "bool":false, "int":1, "float": 0.123} ]}'],
        ];
    }

    public static function invalidProvider(): array
    {
        return [
            ['{'],
            ['}'],
            ['     '],
            ['{"json":"value, "bool":false, "int":1, "float": 0.123, "array": [{"json":"value, "bool":false, "int":1, "float": 0.123}"json":"value, "bool":false, "int":1, "float": 0.123} ]}'],
        ];
    }

    /**
     *
     * @dataProvider positiveProvider
     */
    public function testValidate($input): void
    {
        $this->assertTrue(JsonValidator::validate($input));
    }

    /**
     * @dataProvider invalidProvider
     */
    public function testInvalidJson($input): void
    {
        $this->expectException(\InvalidArgumentException::class);
        JsonValidator::validate($input);
    }

    /**
     * @dataProvider invalidProvider
     */
    public function testInvalidJsonSilent($input): void
    {
        $this->assertFalse(JsonValidator::validate($input, true));
    }
}
