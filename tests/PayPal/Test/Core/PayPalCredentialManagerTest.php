<?php

use PayPal\Auth\OAuthTokenCredential;
use PayPal\Exception\PayPalInvalidCredentialException;
use PayPal\Core\PayPalCredentialManager;
use PHPUnit\Framework\TestCase;

/**
 * Test class for PayPalCredentialManager.
 *
 * @runTestsInSeparateProcesses
 */
class PayPalCredentialManagerTest extends TestCase
{
    /**
     * @var PayPalCredentialManager
     */
    protected $object;

    private array $config = ['acct1.ClientId' => 'client-id', 'acct1.ClientSecret' => 'client-secret', 'http.ConnectionTimeOut' => '30', 'http.Retry' => '5', 'service.RedirectURL' => 'https://www.sandbox.paypal.com/webscr&cmd=', 'service.DevCentralURL' => 'https://developer.paypal.com', 'service.EndPoint.IPN' => 'https://www.sandbox.paypal.com/cgi-bin/webscr', 'service.EndPoint.AdaptivePayments' => 'https://svcs.sandbox.paypal.com/', 'service.SandboxEmailAddress' => 'platform_sdk_seller@gmail.com', 'log.FileName' => 'PayPal.log', 'log.LogLevel' => 'INFO', 'log.LogEnabled' => '1'];

    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function setUp():void
    {
        $this->object = PayPalCredentialManager::getInstance($this->config);
    }


    public function testGetInstance(): void
    {
        $instance = $this->object->getInstance($this->config);
        $this->assertInstanceOf(PayPalCredentialManager::class, $instance);
    }

    public function testGetSpecificCredentialObject(): void
    {
        $cred = $this->object->getCredentialObject('acct1');
        $this->assertNotNull($cred);
        $this->assertSame('client-id', $cred->getClientId());
        $this->assertSame('client-secret', $cred->getClientSecret());
    }

    /**
     * @after testGetDefaultCredentialObject
     *
     * @throws \PayPal\Exception\PayPalInvalidCredentialException
     */
    public function testSetCredentialObject(): void
    {
        $authObject = $this->createMock(OAuthTokenCredential::class);
        $cred = $this->object->setCredentialObject($authObject)->getCredentialObject();

        $this->assertNotNull($cred);
        $this->assertSame($this->object->getCredentialObject(), $authObject);
    }

    /**
     * @after testGetDefaultCredentialObject
     *
     * @throws \PayPal\Exception\PayPalInvalidCredentialException
     */
    public function testSetCredentialObjectWithUserId(): void
    {
        $authObject = $this->createMock(OAuthTokenCredential::class);
        $cred = $this->object->setCredentialObject($authObject, 'sample')->getCredentialObject('sample');
        $this->assertNotNull($cred);
        $this->assertSame($this->object->getCredentialObject(), $authObject);
    }

    /**
     * @after testGetDefaultCredentialObject
     *
     * @throws \PayPal\Exception\PayPalInvalidCredentialException
     */
    public function testSetCredentialObjectWithoutDefault(): void
    {
        $authObject = $this->createMock(OAuthTokenCredential::class);
        $cred = $this->object->setCredentialObject($authObject, null, false)->getCredentialObject();
        $this->assertNotNull($cred);
        $this->assertNotSame($this->object->getCredentialObject(), $authObject);
    }


    public function testGetInvalidCredentialObject(): void
    {
        $this->expectException(PayPalInvalidCredentialException::class);
        $cred = $this->object->getCredentialObject('invalid_biz_api1.gmail.com');
    }

    public function testGetDefaultCredentialObject(): void
    {
        $cred = $this->object->getCredentialObject();
        $this->assertNotNull($cred);

        $this->assertSame('client-id', $cred->getClientId());
        $this->assertSame('client-secret', $cred->getClientSecret());
    }

    public function testGetRestCredentialObject(): void
    {
        $cred = $this->object->getCredentialObject('acct1');

        $this->assertNotNull($cred);
        $this->assertSame($this->config['acct1.ClientId'], $cred->getClientId());
        $this->assertSame($this->config['acct1.ClientSecret'], $cred->getClientSecret());
    }
}
