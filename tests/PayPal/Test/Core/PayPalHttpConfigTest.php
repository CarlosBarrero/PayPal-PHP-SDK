<?php

namespace PayPal\Test\Core;

use PayPal\Core\PayPalHttpConfig;
use PHPUnit\Framework\TestCase;
use PayPal\Exception\PayPalConfigurationException;

/**
 * Test class for PayPalHttpConfigTest.
 *
 */
class PayPalHttpConfigTest extends TestCase
{

    protected $object;

    public function testHeaderFunctions(): void
    {
        $o = new PayPalHttpConfig();
        $o->addHeader('key1', 'value1');
        $o->addHeader('key2', 'value');
        $o->addHeader('key2', 'overwritten');

        $this->assertCount(2, $o->getHeaders());
        $this->assertEquals('overwritten', $o->getHeader('key2'));
        $this->assertNull($o->getHeader('key3'));

        $o = new PayPalHttpConfig();
        $o->addHeader('key1', 'value1');
        $o->addHeader('key2', 'value');
        $o->addHeader('key2', 'and more', false);

        $this->assertCount(2, $o->getHeaders());
        $this->assertEquals('value;and more', $o->getHeader('key2'));

        $o->removeHeader('key2');
        $this->assertCount(1, $o->getHeaders());
        $this->assertNull($o->getHeader('key2'));
    }


    public function testCurlOpts(): void
    {
        $o = new PayPalHttpConfig();
        $o->setCurlOptions(['k' => 'v']);

        $curlOpts = $o->getCurlOptions();
        $this->assertCount(1, $curlOpts);
        $this->assertEquals('v', $curlOpts['k']);
    }

    public function testRemoveCurlOpts(): void
    {
        $o = new PayPalHttpConfig();
        $o->setCurlOptions(['k' => 'v']);
        $curlOpts = $o->getCurlOptions();
        $this->assertCount(1, $curlOpts);
        $this->assertEquals('v', $curlOpts['k']);

        $o->removeCurlOption('k');
        $curlOpts = $o->getCurlOptions();
        $this->assertCount(0, $curlOpts);
    }

    /**
     * @test
     */
    public function testUserAgent(): void
    {
        $ua = 'UAString';
        $o = new PayPalHttpConfig();
        $o->setUserAgent($ua);

        $curlOpts = $o->getCurlOptions();
        $this->assertEquals($ua, $curlOpts[CURLOPT_USERAGENT]);
    }


    public function testSSLOpts(): void
    {
        $sslCert = '../cacert.pem';
        $sslPass = 'passPhrase';

        $o = new PayPalHttpConfig();
        $o->setSSLCert($sslCert, $sslPass);

        $curlOpts = $o->getCurlOptions();
        $this->assertArrayHasKey(CURLOPT_SSLCERT, $curlOpts);
        $this->assertEquals($sslPass, $curlOpts[CURLOPT_SSLCERTPASSWD]);
    }

    public function testProxyOpts(): void
    {
        $proxy = 'http://me:secret@hostname:8081';

        $o = new PayPalHttpConfig();
        $o->setHttpProxy($proxy);

        $curlOpts = $o->getCurlOptions();
        $this->assertEquals('hostname:8081', $curlOpts[CURLOPT_PROXY]);
        $this->assertEquals('me:secret', $curlOpts[CURLOPT_PROXYUSERPWD]);

        $this->expectException(PayPalConfigurationException::class);
        $o->setHttpProxy('invalid string');
    }
}
