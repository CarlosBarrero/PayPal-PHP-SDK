<?php

namespace PayPal\Test\Api;

use ApiContextTest;
use PayPal\Rest\ApiContext;
use PayPal\Api\WebhookEvent;
use PHPUnit\Framework\TestCase;
use PayPal\Transport\PayPalRestCall;

/**
 * Class WebhookEvent
 *
 * @package PayPal\Test\Api
 */
class WebhookEventTest extends TestCase
{
    /**
     * Gets Json String of Object WebhookEvent
     * @return string
     */
    public static function getJson(): string
    {
        return '{"id":"TestSample","create_time":"TestSample","resource_type":"TestSample","event_version":"TestSample","event_type":"TestSample","summary":"TestSample","resource":"TestSampleObject","status":"TestSample","transmissions":"TestSampleObject","links":' .LinksTest::getJson() . '}';
    }

    /**
     * Gets Object Instance with Json data filled in
     * @return WebhookEvent
     */
    public static function getObject(): WebhookEvent
    {
        return new WebhookEvent(self::getJson());
    }


    /**
     * Tests for Serialization and Deserialization Issues
     * @return WebhookEvent
     */
    public function testSerializationDeserialization(): WebhookEvent
    {
        $obj = new WebhookEvent(self::getJson());
        $this->assertNotNull($obj);
        $this->assertNotNull($obj->getId());
        $this->assertNotNull($obj->getCreateTime());
        $this->assertNotNull($obj->getResourceType());
        $this->assertNotNull($obj->getEventVersion());
        $this->assertNotNull($obj->getEventType());
        $this->assertNotNull($obj->getSummary());
        $this->assertNotNull($obj->getResource());
        $this->assertNotNull($obj->getStatus());
        $this->assertNotNull($obj->getTransmissions());
        $this->assertNotNull($obj->getLinks());
        $this->assertEquals(self::getJson(), $obj->toJson());
        return $obj;
    }

    /**
     * @depends testSerializationDeserialization
     * @param WebhookEvent $obj
     */
    public function testGetters($obj): void
    {
        $this->assertEquals($obj->getId(), "TestSample");
        $this->assertEquals($obj->getCreateTime(), "TestSample");
        $this->assertEquals($obj->getResourceType(), "TestSample");
        $this->assertEquals($obj->getEventVersion(), "TestSample");
        $this->assertEquals($obj->getEventType(), "TestSample");
        $this->assertEquals($obj->getSummary(), "TestSample");
        $this->assertEquals($obj->getResource(), "TestSampleObject");
        $this->assertEquals($obj->getStatus(), "TestSample");
        $this->assertEquals($obj->getTransmissions(), "TestSampleObject");
        $this->assertEquals($obj->getLinks(), LinksTest::getObject());
    }

    /**
     * @dataProvider mockProvider
     * @param WebhookEvent $obj
     */
    public function testGet($obj, $mockApiContext): void
    {
        $mockPPRestCall = $this->createMock(PayPalRestCall::class);

        $mockPPRestCall
            ->expects($this->any())
            ->method('execute')
            ->willReturn(self::getJson());

        $result = $obj->get("eventId", $mockApiContext, $mockPPRestCall);
        $this->assertNotNull($result);
    }
    /**
     * @dataProvider mockProvider
     * @param WebhookEvent $obj
     */
    public function testResend($obj, $mockApiContext): void
    {
        $mockPPRestCall = $this->createMock(PayPalRestCall::class);

        $mockPPRestCall
            ->expects($this->any())
            ->method('execute')
            ->willReturn(self::getJson());

        $eventResend = ApiContextTest::getObject();

        $result = $obj->resend($eventResend, $mockPPRestCall);
        $this->assertNotNull($result);
    }
    /**
     * @dataProvider mockProvider
     * @param WebhookEvent $obj
     */
    public function testList($obj, $mockApiContext): void
    {
        $mockPPRestCall = $this->getMockBuilder(PayPalRestCall::class)
            ->disableOriginalConstructor()
            ->getMock();

        $mockPPRestCall
            ->expects($this->any())
            ->method('execute')
            ->willReturn(WebhookEventListTest::getJson());
        $params = [];

        $result = $obj->all($params, $mockApiContext, $mockPPRestCall);
        $this->assertNotNull($result);
    }

    public function mockProvider(): array
    {
        $obj = self::getObject();
        $mockApiContext = $this->createMock(ApiContext::class);
        return [[$obj, $mockApiContext], [$obj, null]];
    }
}
