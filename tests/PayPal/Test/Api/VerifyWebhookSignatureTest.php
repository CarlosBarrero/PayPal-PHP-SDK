<?php

namespace PayPal\Test\Api;

use PayPal\Common\PayPalResourceModel;
use PayPal\Transport\PayPalRestCall;
use PayPal\Validation\ArgumentValidator;
use PayPal\Api\VerifyWebhookSignatureResponse;
use PayPal\Rest\ApiContext;
use PayPal\Api\VerifyWebhookSignature;
use PHPUnit\Framework\TestCase;

/**
 * Class VerifyWebhookSignature
 *
 * @package PayPal\Test\Api
 */
class VerifyWebhookSignatureTest extends TestCase
{
    /**
     * Gets Json String of Object VerifyWebhookSignature
     * @return string
     */
    public static function getJson(): string
    {
        return '{"auth_algo":"TestSample","cert_url":"http://www.google.com","transmission_id":"TestSample","transmission_sig":"TestSample","transmission_time":"TestSample","webhook_id":"TestSample","webhook_event":' .WebhookEventTest::getJson() . '}';
    }

    /**
     * Gets Object Instance with Json data filled in
     * @return VerifyWebhookSignature
     */
    public static function getObject(): VerifyWebhookSignature
    {
        return new VerifyWebhookSignature(self::getJson());
    }


    /**
     * Tests for Serialization and Deserialization Issues
     * @return VerifyWebhookSignature
     */
    public function testSerializationDeserialization(): VerifyWebhookSignature
    {
        $obj = new VerifyWebhookSignature(self::getJson());
        $this->assertNotNull($obj);
        $this->assertNotNull($obj->getAuthAlgo());
        $this->assertNotNull($obj->getCertUrl());
        $this->assertNotNull($obj->getTransmissionId());
        $this->assertNotNull($obj->getTransmissionSig());
        $this->assertNotNull($obj->getTransmissionTime());
        $this->assertNotNull($obj->getWebhookId());
        $this->assertNotNull($obj->getWebhookEvent());
        $this->assertEquals(self::getJson(), $obj->toJson());
        return $obj;
    }

    /**
     * @depends testSerializationDeserialization
     * @param VerifyWebhookSignature $obj
     */
    public function testGetters($obj): void
    {
        $this->assertEquals($obj->getAuthAlgo(), "TestSample");
        $this->assertEquals($obj->getCertUrl(), "http://www.google.com");
        $this->assertEquals($obj->getTransmissionId(), "TestSample");
        $this->assertEquals($obj->getTransmissionSig(), "TestSample");
        $this->assertEquals($obj->getTransmissionTime(), "TestSample");
        $this->assertEquals($obj->getWebhookId(), "TestSample");
        $this->assertEquals($obj->getWebhookEvent(), WebhookEventTest::getObject());
    }

    public function testUrlValidationForCertUrl()
    {
        $this->expectException(\InvalidArgumentException::class);
        $this->expectExceptionMessage("CertUrl is not a fully qualified URL");
        $obj = new VerifyWebhookSignature();
        $obj->setCertUrl(null);
    }

    public function testToJsonToIncludeRequestBodyAsWebhookEvent(): void
    {
        $obj = new VerifyWebhookSignature();
        $requestBody = '{"id":"123", "links": [], "something": {}}';
        $obj->setRequestBody($requestBody);

        $this->assertEquals($obj->toJSON(), '{"webhook_event": ' . $requestBody .'}');
    }

    /**
     * @dataProvider mockProvider
     * @param VerifyWebhookSignature $obj
     */
    public function testPost($obj, $mockApiContext): void
    {
        $mockPPRestCall = $this->createMock(PayPalRestCall::class);

        $mockPPRestCall
            ->expects($this->any())
            ->method('execute')
            ->willReturn(VerifyWebhookSignatureResponseTest::getJson());

        $result = $obj->post($mockApiContext, $mockPPRestCall);
        $this->assertNotNull($result);
    }

    public function mockProvider(): array
    {
        $obj = self::getObject();
        $mockApiContext = $this->getMockBuilder('ApiContext')
                    ->disableOriginalConstructor()
                    ->getMock();
        return [[$obj, $mockApiContext], [$obj, null]];
    }
}
