<?php

namespace PayPal\Test\Api;

use PayPal\Common\PayPalResourceModel;
use PayPal\Transport\PayPalRestCall;
use PayPal\Validation\ArgumentValidator;
use PayPal\Api\WebhookList;
use PayPal\Rest\ApiContext;
use PayPal\Api\Webhook;
use PHPUnit\Framework\TestCase;

/**
 * Class Webhook
 *
 * @package PayPal\Test\Api
 */
class WebhookTest extends TestCase
{
    /**
     * Gets Json String of Object Webhook
     * @return string
     */
    public static function getJson(): string
    {
        return '{"id":"TestSample","url":"http://www.google.com","event_types":' .WebhookEventTypeTest::getJson() . ',"links":' .LinksTest::getJson() . '}';
    }

    /**
     * Gets Object Instance with Json data filled in
     * @return Webhook
     */
    public static function getObject(): Webhook
    {
        return new Webhook(self::getJson());
    }


    /**
     * Tests for Serialization and Deserialization Issues
     * @return Webhook
     */
    public function testSerializationDeserialization(): Webhook
    {
        $obj = new Webhook(self::getJson());
        $this->assertNotNull($obj);
        $this->assertNotNull($obj->getId());
        $this->assertNotNull($obj->getUrl());
        $this->assertNotNull($obj->getEventTypes());
        $this->assertNotNull($obj->getLinks());
        $this->assertEquals(self::getJson(), $obj->toJson());
        return $obj;
    }

    /**
     * @depends testSerializationDeserialization
     * @param Webhook $obj
     */
    public function testGetters($obj): void
    {
        $this->assertEquals($obj->getId(), "TestSample");
        $this->assertEquals($obj->getUrl(), "http://www.google.com");
        $this->assertEquals($obj->getEventTypes(), WebhookEventTypeTest::getObject());
        $this->assertEquals($obj->getLinks(), LinksTest::getObject());
    }

    public function testUrlValidationForUrl(): void
    {
        $this->expectException(\InvalidArgumentException::class);
        $this->expectExceptionMessage("Url is not a fully qualified URL");
        $obj = new Webhook();
        $obj->setUrl(null);
    }
    /**
     * @dataProvider mockProvider
     * @param Webhook $obj
     */
    public function testCreate($obj, $mockApiContext): void
    {
        $mockPPRestCall = $this->createMock(PayPalRestCall::class);

        $mockPPRestCall->expects($this->any())
            ->method('execute')
            ->willReturn(self::getJson());

        $result = $obj->create($mockApiContext, $mockPPRestCall);
        $this->assertNotNull($result);
    }
    /**
     * @dataProvider mockProvider
     * @param Webhook $obj
     */
    public function testGet($obj, $mockApiContext): void
    {
        $mockPPRestCall = $this->createMock(PayPalRestCall::class);

        $mockPPRestCall->expects($this->any())
            ->method('execute')
            ->willReturn(WebhookTest::getJson());

        $result = $obj->get("webhookId", $mockApiContext, $mockPPRestCall);
        $this->assertNotNull($result);
    }
    /**
     * @dataProvider mockProvider
     * @param Webhook $obj
     */
    public function testGetAll($obj, $mockApiContext): void
    {
        $mockPPRestCall = $this->createMock(PayPalRestCall::class);

        $mockPPRestCall->expects($this->any())
            ->method('execute')
            ->willReturn(WebhookListTest::getJson());
        $params = [];

        $result = $obj->getAllWithParams($params, $mockApiContext, $mockPPRestCall);
        $this->assertNotNull($result);
    }
    /**
     * @dataProvider mockProvider
     * @param Webhook $obj
     */
    public function testUpdate($obj, $mockApiContext): void
    {
        $mockPPRestCall = $this->createMock(PayPalRestCall::class);

        $mockPPRestCall
            ->expects($this->any())
            ->method('execute')
            ->willReturn(self::getJson());
        $patchRequest = PatchRequestTest::getObject();

        $result = $obj->update($patchRequest, $mockApiContext, $mockPPRestCall);
        $this->assertNotNull($result);
    }
    /**
     * @dataProvider mockProvider
     * @param Webhook $obj
     */
    public function testDelete($obj, $mockApiContext): void
    {
        $mockPPRestCall = $this->createMock(PayPalRestCall::class);

        $mockPPRestCall->expects($this->any())
            ->method('execute')
            ->willReturn(true);

        $result = $obj->delete($mockApiContext, $mockPPRestCall);
        $this->assertNotNull($result);
    }

    public function mockProvider(): array
    {
        $obj = self::getObject();
        $mockApiContext = $this->getMockBuilder('ApiContext')
                    ->disableOriginalConstructor()
                    ->getMock();
        return [[$obj, $mockApiContext], [$obj, null]];
    }
}
