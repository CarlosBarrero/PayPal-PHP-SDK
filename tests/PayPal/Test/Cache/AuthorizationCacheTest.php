<?php

namespace PayPal\Test\Cache;

use PayPal\Cache\AuthorizationCache;
use PHPUnit\Framework\TestCase;


class AuthorizationCacheTest extends TestCase
{
    final public const CACHE_FILE = 'tests/var/test.cache';

    public static function EnabledProvider(): array
    {
        return [[['cache.enabled' => 'true'], true], [['cache.enabled' => true], true]];
    }

    public static function CachePathProvider()
    {
        return [[['cache.FileName' => 'temp.cache'], 'temp.cache']];
    }

    /**
     *
     * @dataProvider EnabledProvider
     */
    public function testIsEnabled($config, $expected): void
    {
        $result = AuthorizationCache::isEnabled($config);
        $this->assertEquals($expected, $result);
    }

    /**
     * @dataProvider CachePathProvider
     */
    public function testCachePath($config, $expected): void
    {
        $result = AuthorizationCache::cachePath($config);
        $this->assertStringContainsString($expected, $result);
    }

    public function testCacheDisabled(): void
    {
        $this->markTestSkipped();
        // 'cache.enabled' => true,
        AuthorizationCache::push(['cache.enabled' => false], 'clientId', 'accessToken', 'tokenCreateTime', 'tokenExpiresIn');
        AuthorizationCache::pull(['cache.enabled' => false], 'clientId');
    }

    public function testCachePush(): void
    {
        AuthorizationCache::push(['cache.enabled' => true, 'cache.FileName' => AuthorizationCacheTest::CACHE_FILE], 'clientId', 'accessToken', 'tokenCreateTime', 'tokenExpiresIn');
        $contents = file_get_contents(AuthorizationCacheTest::CACHE_FILE);
        $tokens = json_decode($contents, true, 512, JSON_THROW_ON_ERROR);
        $this->assertNotNull($contents);
        $this->assertEquals('clientId', $tokens['clientId']['clientId']);
        $this->assertEquals('accessToken', $tokens['clientId']['accessTokenEncrypted']);
        $this->assertEquals('tokenCreateTime', $tokens['clientId']['tokenCreateTime']);
        $this->assertEquals('tokenExpiresIn', $tokens['clientId']['tokenExpiresIn']);
    }

    public function testCachePullNonExisting(): void
    {
        $result = AuthorizationCache::pull(['cache.enabled' => true, 'cache.FileName' => AuthorizationCacheTest::CACHE_FILE], 'clientIdUndefined');
        $this->assertNull($result);
    }

    /**
     * @depends testCachePush
     */
    public function testCachePull(): void
    {
        $result = AuthorizationCache::pull(['cache.enabled' => true, 'cache.FileName' => AuthorizationCacheTest::CACHE_FILE], 'clientId');
        $this->assertNotNull($result);
        $this->assertIsArray($result);
        $this->assertEquals('clientId', $result['clientId']);
        $this->assertEquals('accessToken', $result['accessTokenEncrypted']);
        $this->assertEquals('tokenCreateTime', $result['tokenCreateTime']);
        $this->assertEquals('tokenExpiresIn', $result['tokenExpiresIn']);

        unlink(AuthorizationCacheTest::CACHE_FILE);
    }
}
